# RSPocketBook

The RSPocketBook was originally founded (designed, developed and produced) by Blair Quinn and Rhys Dyson in 2017. The PocketBook took the form of an interactive PDF, hosted on the web. Rhys began to convert the PDF into this website platform between 2019-2020.

This website has been opened up to the community on GitLab, to allow others to contribute to content updates and to source additional developers to help de-bug and improve the site.

By accessing this website, you accept and agree to the terms of our [License](https://gitlab.com/rspocketbook/website/-/blob/master/LICENSE.md) and our [Privacy Policy](https://gitlab.com/rspocketbook/website/-/blob/master/PRIVACY-POLICY.md).

## Additional Information

Want to become a developer and/or contribute to the website?

There are two methods. You can either edit the information on the pages through the repository on gitlab.com, or download the repository and work on the website locally.
follow our [developer guidelines](https://gitlab.com/rspocketbook/website/-/blob/master/DEVELOPER-GUIDELINES.md) for instructions on how to get started.