# Privacy policy

The decision to develop the RSPocketBook as a static website was intentional. With no databases, we wouldn't require use accounts, which significantly reduces the personal data collected which is typically required to offer such services.

You may however be tracked throughout this website through the use of Google Analytics. This data helps us analyse which content is most useful to our audience, so we can prioritise updates towards the most accessed pages first. In addition, we can monitor how successful the project is going.

Data collected is very general, and cannot be tracked back to yourself. Names, contact details and other forms of identifications cannot be collected nor accessed by our team.

Some of the data we have access to includes:

- Quantity of total page views and accesses,
- The location traffic has come from (direct search engine searches, adverts, directly using our URL),
- The quantity of returning users,
- Country and region of our users.

Please note that by using GitLab, you are bound by the privacy terms and licenses of those services. Users of our repository will have access to a list of any and all edits or "commits" you make to our repository. Therefore your username, profile picture and other information that you have made public will be accessible.

The RSPocketBook will never request access to your personal information, your RuneScape account or any other personal information unless you have directly contacted us first, and we require this information for communicative purposes.
