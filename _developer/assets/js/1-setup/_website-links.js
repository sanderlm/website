// Find all hyperlinks with target="_blank"
// and adds rel="noopener noreferrer" for security.

$(document).ready(function(){
	$('a').each(function(){
		if ( $(this).attr('target') == '_blank'){
			$(this).attr('rel','noopener noreferrer');
		}
	})

});

// Find all hyperlinks and adds rel="noopener" for security.

$(document).ready(function(){

	$('a').each(function(){
		$(this).attr('rel','noopener');
	})

});