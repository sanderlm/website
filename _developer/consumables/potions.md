---
layout: page
title: Potions
---

## Introduction

Potions are created with the Herblore skill and provide various powerful effects. Potions can be used to provide skill/stat boosts, heal, restore; prayer, summoning, adrenaline and more.

#### Potion Containers

- Players with lower Herblore levels should convert their potions (4) into flasks (6) to carry extra dosages into combat.
- Combination potions combine and enhance the effects of potions into a crystal flask (6). High level players who have access to Prifddinas should make combination potions as soon as possible. Combination recipes are discovered in Daemonheim while Dungeoneering and craftable after paying Lady Meilyr a fee.

## Combat Boosting Potions

Combat potions provide a 6-minute boost to attack, strength, ranged, magic and defence. Overloads increase all stats at once, with a base level increase plus a percentage of that players' level. 

There are currently three strengths of overload:
1. Overload: 15% + 3 levels
2. Supreme overload: 16% + 4 levels
3. Elder overload: 17% + 5 levels

A salve combines one of these types of overload with a prayer and renewal potion, antifire and super antifire, as well as an antipoison potion to deliver the effects of almost all potions at once.

{% include tables/consumables/combat-potions.html %}

## Slayer Combination Potions

The combination potions below are alternative options to salves which can be used during slayer and lower level PvM. They provide a cheaper alternative to salves, are ueful when all salve buffs aren't required (such as using a demonhorn necklace to restore prayer), and lower level alternatives for those without a high herblore level.

{% include tables/consumables/slayer-potions.html %}

## Replenishment Potions

Replenishment potions restore health, prayer, summoning, adrenaline and offer cures for poison. Many of these potion effects are bundled into salves and are therefore useful when you either aren't using or don't have access to salves.

{% include tables/consumables/replenishment-potions.html %}