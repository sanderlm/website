---
layout: page
title: Bleed Abilities
---

## Introduction

Bleeds are damage-over-time and (for the sake of simplicity) heal-over-time abilities, that once placed, passively damage or heal a target. Due to their mechanics, bleeds often require strict communication in group PvM situations to maximise their efficiency. Thus, bleeds shouldn’t be a priority for everyone in a public group boss, but should still be used often, especially if you are one of the only DPS’s using a specific combat style. These are due to a few factors, which associate with many abilities.

## Bleeds that Do Not Stack

These abilities do not stack when multiple players cast them on a target at the same time. If a person casts the same bleed before the previous expires, it cancels the previous, and the bleed’s duration may or may not be renewed. As a result, it is important to have good team communication when using these abilities.

{% include tables/guides/abilities/bleeds/wont-stack.html %}

Bleeds don’t get increased damage from damage-boosting Ultimate abilities, and for that reason shouldn’t be in your Ultimate rotation. Corruption Shot/Blast are exceptions due to their high basic damage and should still be used in group and ultimate rotations.

## Bleeds that Stack

{% include tables/guides/abilities/bleeds/will-stack.html %}

Greater Dazing Shot is stackable and great to use in group bossing.

Debilitate will not cancel when multiple players use it on a target, however the damage reduction is personal so you must cast it to receive it.

Shards should only be used to stall and build Adrenalin when a boss is un-targetable.
