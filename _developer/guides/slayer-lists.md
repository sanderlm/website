---
layout: page
title: Slayer Lists
---

## Introduction

Slayer lists refer to the prefer and block lists which can be modified through the slayer reward shop at any slayer master. These increase and decrease the chances of rolling specific slayers tasks respectively. There are three lists optimised for maximising slayer xp rates, combat xp rates, and an easier lenient list which is a good middle ground between the two.

To view the relevant armour sets optimised for defeating slayer creatures, view our [slayer armour sets]({{ site.baseurl }}armour/slayer.html).

## Prefer Lists

### Slayer XP

{% include tables/guides/slayer-lists/slayer.html %}

### Combat XP

{% include tables/guides/slayer-lists/combat.html %}

### Lenient Tasks

{% include tables/guides/slayer-lists/lenient.html %}

## Block Lists

{% include tables/guides/slayer-lists/block-lists.html %}
