---
layout: page
title: Bar 1 - Full Automatic Ability Bars
---

## Introduction

The abilities listed in the table below are related to the bar 1 keybinds on the keybinds page. These ability bars are optimised for players using full automatic (auto casting all types of abilities).

{% include tables/guides/ability-bars/full-automatic.html %}
