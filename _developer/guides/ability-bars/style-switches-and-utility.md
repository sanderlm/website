---
layout: page
title: Bar 5 - Style Specific Switches and Abilities
---

## Introduction

The abilities listed in the table below are related to the bar 5 keybinds on the keybinds page. These ability bars are manually cast, with a setup specific to each combat style.

These bars make weapon switches easier, and give players access to the threshold and ultimate abilities they won't commonly use. As a result these bars are mostly helpful to players that have multiple weapon switches, and could be combined into a single bar for those that only have a few.

{% include tables/guides/ability-bars/style-switches-and-utility.html %}
