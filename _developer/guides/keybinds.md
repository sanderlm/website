---
layout: page
title: Keybinds
---

## Introduction

Players can display a maximum of 5 ability bars on screen at any one time. This includes the primary ability bar (attached to the health and status bars) and the "additional action bars" 1 through to 4. This amount of ability bars allows players to keybind up to 70 abilities at once, providing adequate room for all of the combat styles, abilities and items all at once.

The keybinds found on this page are designed to coincide with our recommended ability bar setup which dedicates bars to one or all of the combat styles, prayers and consumables, defenses, weapons/item switches and additional style specific abilities. Please note, that this is not a one size fits all approach, and you may find some tweaking and changes required to your liking.

To gain a better understanding of this setup or view our ability bars, please view our [ability bars page]({{ site.baseurl }}guides/ability-bars.html).

## Action Bar Binding

Action bars can be set to switch automatically depending on which weapon is equipped. This can be achieved through the settings menu > combat > action bar binding. Select an action bar (the on screen slot to change), the action bar preset (the one to change it to), and the combat style or weapon to trigger than change.

![image]({{ site.baseurl }}assets/img/game-ui/action-bar-binding.png){:style="display: block; margin: 0 auto; width: 400px; height: auto;"}

## Keyboard Binds

These ability bars revolve around the QWER home keys, where the fingers should rest.

- You should pretty much never need to hit buttons 7 & 8. They are primarily there so revolution should run through it correctly.

- If you don’t intend to use bar 5, switch Planted Feet to another bar, but keep the bind for Planted Feet the same.

#### Binds outside ability bars:

- Quick Prayers should be bound to ‘P’ in the controls menu.

- Summoning left click option should be bound in the menu to ‘N’.

- Weapon Special Attack should be bound to 'Shift + 6' for keyboard, or 'Page Down' for mouse.

- Use an external program to rebind Caps Lock, and then bind Target Cycle to Caps Lock in your menu.

## Mouse Binds

RuneScape does not natively support mouse keybinds, so a proper gaming mouse is likely required that utilises the Insert, Home, PageUp etc. control keys, managed by external software (used at your own risk).

A gaming mouse is no requirement for keybinding, as mouse binds simply replace keyboard binds. However, a 12-button mouse is best to save keyboard travel, and make weapon switches and offensive abilites faster to cast. ![image]({{ site.baseurl }}assets/img/infographic/mouse-buttons.png){:style="display: block; margin: 0 auto; width: 400px; height: auto;"}

## Keybinds

The table below outlines our recommended keybinds for all 5 onscreen ability bars (Bar 5 optional). The keys wrapped in [ square brackets ] are a reference to mouse button alternatives.

{% include tables/guides/keybinds.html %}

### Reminder:

- The 12th/missing mouse keybind ‘Page Down’ is used for the weapon special attack (bound through the keybinds interface).