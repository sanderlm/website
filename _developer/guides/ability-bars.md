---
layout: page
title: Ability Bars
---

## Introduction

Ability bars or action bars are 14-celled tables which players can add combat abilities, prayers, spells and items to in-order to assign a keybind for quick access. Members can have up to thirteen saved ability bars to rotate though, while free-to-play players can access seven. These additional ability bars can be purchased from Solomon's General Store for 195 RuneCoins.

To learn about the keybinds assigned to these ability bars, please view our [keybinds page]({{ site.baseurl }}guides/keybinds.html).

## Our Ability Bar Overview

When enabled, players can display a total of 5 ability bars on screen at any one time. Our recommended setup utilises all 5 of these onscreen action bars. Bar 1 is the primary bar for casting abilities, and players should have three versions for each combat style. In addition, they may want an alternative set for different combat modes such as full manual or revolution.

{% include tables/guides/ability-bars.html %}

## Bar 5

Bar 5 is optional, providing keybind options for style specific weapon switches, and all other ability alternatives outside the primary bar (such as extra ultimates). As with Bar 1, this can have three alternatives for each combat style.

## Displaying Additional Ability Bars

To display all five ability bars on screen, press Escape to open the game settings, select gameplay > combat > action bar. Down the bottom, you can select which action bar will be active in each onscreen ability bar slot.

![image]({{ site.baseurl }}assets/img/game-ui/additional-ability-bars.png){:style="display: block; margin: 0 auto; width: 400px; height: auto;"}

Here, the player uses bars 1-3 for revolution, 4-6 for full automatic, 7-9 for utility, and 10-12 for style specific switches. Therefore they permanently display the utility bars 7-9, and either 10, 11 or 12 depending on combat mode.
