---
layout: page
title: Slayer Power Armour Sets
---

## Introduction

Slayer armour sets are specifically set up to deal high damage to slayer creatures. As a result, they strictly consist of power armour sets (rather than tank sets) and are equipped with perks that increase damage to specific types of slayer monsters.

To optimise your experience gains in slayer, view our [slayer task lists]({{ site.baseurl }}guides/slayer-lists.html).

## Slayer Armour Sets

Power armour sets are commonly used for slayer as damage output is far more important than defensive stats during tasks. Tier 70 power sets are popular options as they cost less to maintain than the tier 80 or 90 options while still providing adequate DPS output.

Custom-fit Trimmed Masterwork does not degrade during slayer tasks but is rather expensive to obtain. This powerful armour set is more beneficial when optimised for bossing, however players may choose to gather a second set tailored towards slayer.

{% include tables/armour/power-sets.html %}

## Slayer Armour Perks

The perks outlined below are optimised to provide a DPS boost towards slayer creatures. The torso of all combat styles remain the same, containing B2G + C3 (with or without Crystal Shield). The legs for each style are setup for various purposes. Melee is setup for killing demons (or scavenging) while ranged is optimised for Demon and Dragon slaying.
+-*/
{% include tables/armour/slayer-perks.html %}

### Note:

1. Switch out Demon Slayer on the Melee legs for Scavenging 3 you don't intend on killing demons, or adding them to your prefer list.
2. Switch out Impatient 3 on the Ranged legs for Scavenging 3 if you prefer the material collection over the increased DPS.

- Zamorak Mjolnirs can be collected for free Zamorak components ([Guide](https://www.youtube.com/watch?v=pBmaJYu44lA)).