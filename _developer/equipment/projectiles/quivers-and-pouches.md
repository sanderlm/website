---
layout: page
title: Quivers and Pouches
---

## Introduction

Quivers and Pouches hold ranged projectiles and runes respectively. The Tirannwn Quiver holds a single ranged ammunition type (arrows or bolts) and provides a small prayer bonus. Rune pouches on the other hand can hold multiple types of runes, but provide no stat bonuses.

## Quivers and Pouches

{% include tables/projectiles/quivers-and-pouches.html%}
