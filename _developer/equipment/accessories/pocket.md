---
layout: page
title: Pocket
---

## Introduction

The pocket slot can store a wide range of passive ability items. The most common items placed here are Scrimshaws and Books, to provide various combat effects.

## Scrimshaws

Scrimshaws originate from Player Owned Ports and can be gathered from the minigame. Additionally, an inferior version can be purchased from the G.E. Scrimshaws provide benefits for all combat styles, with increased damage for Ranged and Magic and life steal effects for Melee.

{% include tables/accessories/pocket/scrimshaws.html %}

## Books

Books provide passive damage increases and additional effects, and can be used in a wide range of scenarios.

{% include tables/accessories/pocket/books.html %}

## Blood Essence

Blood Essence can be created after the River of Blood quest. There are other types of Blood Essence, but Berserker Blood essence is arguably the most useful.

{% include tables/accessories/pocket/blood-essence.html %}
