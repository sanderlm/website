---
layout: page
title: Auras
---

## Introduction

Auras are powerful temporary effects which can typically be purchased from Solomon's General store for Loyalty Points. Once purchased from Solomon, the player has access to these Auras permanently. They usually last up to an hour, and take several hours recharge/cooldown and become usable again.

## Damage Auras

The table below outlines a series of high damage dealing Auras. These are best saved for bosses throughout the day, but can also be used during slayer tasks.

{% include tables/accessories/auras/damage.html %}

## Accuracy Auras

Accuracy auras are great alternative DPS auras for when the above auras are on cooldown, or you are unable to achieve 100% accurate hits.

{% include tables/accessories/auras/accuracy.html %}

## Regeneration Auras

Regeneration Auras provide prayer, health and adrenaline effects during combat.

{% include tables/accessories/auras/regeneration.html %}
