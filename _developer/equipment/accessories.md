---
layout: page
title: Accessories
---

## Amulets

Amulets are worn in the neck slot and provide various stat bonuses and effects.

[Amulets](/equipment/accessories/amulets.html){: .button}

## Auras

Auras are tempoary effects activated through the inventory interface and displayed in the aura slot.

[Auras](/equipment/accessories/auras.html){: .button}

## Pocket

The pocket slot is a place to store utility items for combat providing a wide range of effects.

[Pocket](/equipment/accessories/pocket.html){: .button}

## Rings

Rings are worn in the ring slot and provide various stat bonuses and effects.

[Rings](/equipment/accessories/rings.html){: .button}

## Tool Belt

The tool belt slot is a place to store various automated effects and upgrades.

[Tool Belt](/equipment/accessories/tool-belt.html){: .button}

## Specialty Tools

Specialty tools are additional items that can be helpful in the players inventory.

[Specialty Tools](/equipment/accessories/specialty.html){: .button}