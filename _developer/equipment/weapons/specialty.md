---
layout: page
title: Specialty Weapons
---

## Introduction

Specialty weapons are used for their unique effects such as affinity debuffs or damage boosts against particular creatures. Most of these weapons are quest or mini-quest/event rewards. test

## Specialty Weapons

{% include tables/weapons/specialty.html %}
